#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <functional>
#include <numeric>
#include <fstream>
#include <iterator>
#include <list>
#include <stdexcept>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

using Results = std::vector<StatResult>;
using Data = std::vector<double>;

using Statistics = std::function<void (const Data&, Results&)>;

class DataAnalyzer
{
    Statistics statistics_;
    Data data_;
	Results results_;
public:
    DataAnalyzer(Statistics statistics) : statistics_{statistics}
	{
	}

	void load_data(const std::string& file_name)
	{
		data_.clear();
		results_.clear();

        data_ = parse_input_file(file_name);

		std::cout << "File " << file_name << " has been loaded...\n";
	}

    void set_statistics(Statistics statistics)
	{
        statistics_ = statistics;
	}

	void calculate()
	{
        statistics_(data_, results_);
	}

	const Results& results() const
	{
		return results_;
	}
protected:
    virtual Data parse_input_file(const std::string& file_name)
    {
        Data data;

        std::ifstream fin(file_name.c_str());
        if (!fin)
            throw std::runtime_error("File not opened");

        double d;
        while (fin >> d)
        {
            data.push_back(d);
        }

        return data;
    }
};

struct Avg
{
    void operator()(const Data& data, Results& results) const
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        StatResult result("AVG", avg);
        results.push_back(result);
    }
};

struct MinMax
{
    void operator()(const Data& data, Results& results) const
    {
        double min = *(std::min_element(data.begin(), data.end()));
        double max = *(std::max_element(data.begin(), data.end()));

        results.push_back(StatResult("MIN", min));
        results.push_back(StatResult("MAX", max));
    }
};

void show_results(const Results& results)
{
    for(const auto& rslt : results)
		std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
    Avg AVG;
    MinMax MINMAX;

    auto SUM = [](auto& data, auto& results)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        results.push_back(StatResult("SUM", sum));
    };

	DataAnalyzer da {AVG};
	da.load_data("data.dat");

	da.calculate();

	da.set_statistics(MINMAX);
	da.calculate();

	da.set_statistics(SUM);
	da.calculate();

	show_results(da.results());

	std::cout << "\n\n";

	da.load_data("new_data.dat");
	da.calculate();

	show_results(da.results());
}
