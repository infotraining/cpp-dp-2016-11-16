#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <memory>
#include <functional>
#include <sstream>

const double normal_interest_rate = 0.05;
const double overdraft_interest_rate = 0.15;

struct AccountContext
{
    int id;
    double balance;
};

class AccountState
{
public:
    virtual AccountState const* withdraw(AccountContext& context, double amount) const = 0;
    virtual AccountState const* deposit(AccountContext& context, double amount) const = 0;
    virtual void pay_interest(AccountContext& context) const = 0;
    virtual std::string status() const = 0;
    virtual ~AccountState() = default;
};

class AccountStateBase : public AccountState
{
    double interest_;
public:
    AccountStateBase(double interest) : interest_{interest}
    {}

    void pay_interest(AccountContext& context) const override
    {
        context.balance += context.balance * interest_;
    }
};

class NormalState : public AccountStateBase
{
public:
    NormalState(double interest = normal_interest_rate) : AccountStateBase{interest}
    {}

    AccountState const* withdraw(AccountContext &context, double amount) const;

    AccountState const* deposit(AccountContext &context, double amount) const
    {
        context.balance += amount;

        return this;
    }

    std::string status() const
    {
        return "Normal";
    }
};

class OverdraftState : public AccountStateBase
{
public:
    OverdraftState(double interest = overdraft_interest_rate)
        : AccountStateBase{interest}
    {}

    AccountState const* withdraw(AccountContext &context, double amount) const;

    AccountState const* deposit(AccountContext &context, double amount) const;

    std::string status() const
    {
        return "Overdraft";
    }
};

class BankAccount
{
    AccountContext context_ = {};
    AccountState const* state_;

public:
    static const NormalState normal_state;
    static const OverdraftState overdraft_state;

    BankAccount(int id) : context_ {id, 0.0}, state_(&BankAccount::normal_state) {}

	void withdraw(double amount)
	{
		assert(amount > 0);

        state_ = state_->withdraw(context_, amount);
	}

	void deposit(double amount)
	{
		assert(amount > 0);

        state_ = state_->deposit(context_, amount);
	}

	void pay_interest()
	{
        state_->pay_interest(context_);
	}

    std::string status() const
	{
        std::stringstream strm;
        strm << "BankAccount #" << context_.id
             << "; State: " << state_->status()
             << "; Balance: " << context_.balance;

        return strm.str();
	}

	double balance() const
	{
        return context_.balance;
	}

	int id() const
	{
        return context_.id;
	}
};

#endif
