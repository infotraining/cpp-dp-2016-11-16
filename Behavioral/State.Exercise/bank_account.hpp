#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <memory>
#include <functional>
#include <sstream>

struct AccountContext
{
    int id_;
    double balance_;
};

class AccountState
{
public:
    virtual void withdraw(AccountContext& data_context, double amount) const = 0;
    virtual void pay_interest(AccountContext& data_context) const = 0;
    virtual std::string status() const = 0;
    virtual ~AccountState() = default;
};

class AccountStateBase : public AccountState
{
    const double interest_rate_;
public:
    AccountStateBase(double interest_rate) : interest_rate_{interest_rate}
    {}

    double interest_rate() const
    {
        return interest_rate_;
    }

    void pay_interest(AccountContext& data_context) const override
    {
        data_context.balance_ *= (1.0 + interest_rate_);
    }
};

class NormalState : public AccountStateBase
{
public:
    using AccountStateBase::AccountStateBase;

    void withdraw(AccountContext& data_context, double amount) const override
    {
        data_context.balance_ -= amount;
    }

    std::string status() const override
    {
        return "Normal";
    }
};

class OverdraftState : public AccountStateBase
{
public:
    using AccountStateBase::AccountStateBase;

    void withdraw(AccountContext& data_context, double amount) const override
    {
        std::cout << "Brak srodkow na koncie #" << data_context.id_ <<  std::endl;
    }

    std::string status() const override
    {
        return "Overdraft";
    }
};

class BankAccount
{
    AccountContext data_context_;
    AccountState const * state_;

    static const NormalState normal_;
    static const OverdraftState overdraft_;
protected:
	void update_account_state()
	{
        if (data_context_.balance_ < 0)
            state_ = &overdraft_;
		else
            state_ = &normal_;
	}
public:
    BankAccount(int id) : data_context_{id, 0.0}, state_{&normal_}
    {}

	void withdraw(double amount)
	{
		assert(amount > 0);

        state_->withdraw(data_context_, amount);

        update_account_state();
	}

	void deposit(double amount)
	{
		assert(amount > 0);

        data_context_.balance_ += amount;

		update_account_state();
	}

	void pay_interest()
	{
        state_->pay_interest(data_context_);
	}

    std::string status() const
	{
        std::stringstream strm;
        strm << "BankAccount #" << data_context_.id_ << "; State: " << state_->status()
             << " Balance: " << data_context_.balance_;

        return strm.str();
	}

	double balance() const
	{
        return data_context_.balance_;
	}

	int id() const
	{
        return data_context_.id_;
	}
};

#endif
