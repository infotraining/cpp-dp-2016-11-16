#include <iostream>
#include <memory>
#include <cassert>
#include <typeinfo>

using namespace std;

class Engine
{
public:
    virtual void start() = 0;
    virtual void stop() = 0;

    std::unique_ptr<Engine> clone() const
    {
        std::unique_ptr<Engine> cloned_engine = do_clone();
        assert(typeid(*cloned_engine) == typeid(*this));

        return cloned_engine;
    }

    virtual ~Engine() = default;
protected:
    virtual std::unique_ptr<Engine> do_clone() const = 0;
};

// CRTP
template <typename Type, typename BaseType = Engine>
class CloneableEngine : public BaseType
{
protected:
    std::unique_ptr<Engine> do_clone() const override
    {
        return std::make_unique<Type>(static_cast<const Type&>(*this));
    }
};

class Diesel : public CloneableEngine<Diesel>
{
public:
    virtual void start() override
    {
        cout << "Diesel starts\n";
    }

    virtual void stop() override
    {
        cout << "Diesel stops\n";
    }
};

class TDI : public CloneableEngine<TDI, Diesel>
{
public:
    virtual void start() override
    {
        cout << "TDI starts\n";
    }

    virtual void stop() override
    {
        cout << "TDI stops\n";
    }       
};

class Hybrid : public CloneableEngine<Hybrid>
{
public:
    virtual void start() override
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop() override
    {
        cout << "Hybrid stops\n";
    }
};

class Car
{
    unique_ptr<Engine> engine_;
public:
    Car(unique_ptr<Engine> engine) : engine_{ move(engine) }
    {}

    Car(const Car& source) : engine_{ source.engine_->clone() }
    {
    }

    Car& operator=(const Car& source)
    {
        if (this != &source)
        {
            engine_ = source.engine_->clone();
        }

        return *this;
    }

    Car(Car&&) = default;
    Car& operator=(Car&&) = default;

    void drive(int km)
    {
        engine_->start();
        cout << "Driving " << km << " kms\n";
        engine_->stop();
    }
};

int main()
{
    Car c1{ make_unique<TDI>() };
    c1.drive(100);

    cout << "\n";

    Car c2 = c1;
    c2.drive(200);

    cout << "\n";

    Car c3 = move(c2);

}

