#ifndef RAPORT_BUILDER_HPP
#define RAPORT_BUILDER_HPP

#include <string>
#include <vector>
#include <memory>

using DataRow = std::vector<std::string>;
using HtmlDocument = std::string;
using CsvDocument = std::vector<std::string>;

class ReportBuilder
{
public:
    ReportBuilder() {}
    virtual ReportBuilder& add_header(const std::string& header_text) = 0;
    virtual ReportBuilder& begin_data() = 0;
    virtual ReportBuilder& add_row(const DataRow& data_row) = 0;
    virtual ReportBuilder& end_data() = 0;
    virtual ReportBuilder& add_footer(const std::string& footer) = 0;

    virtual ~ReportBuilder() = default;
};


class HtmlReportBuilder : public ReportBuilder
{

    ReportBuilder& add_header( const std::string& header_text ) override;
    ReportBuilder& begin_data() override;
    ReportBuilder& add_row( const DataRow& data_row ) override;
    ReportBuilder& end_data() override;
    ReportBuilder& add_footer( const std::string& footer ) override;
public:
	HtmlDocument get_report();
private:
	HtmlDocument doc_;
};

class CsvReportBuilder : public ReportBuilder
{
    ReportBuilder& add_header(const std::string& header_text ) override;
    ReportBuilder& begin_data() override;
    ReportBuilder& add_row(const DataRow& data_row) override;
    ReportBuilder& end_data() override;
    ReportBuilder& add_footer(const std::string& footer) override;
public:
	CsvDocument get_report();
private:
	CsvDocument doc_;
};

#endif // RAPORT_BUILDER_HPP



