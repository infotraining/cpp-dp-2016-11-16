#ifndef SHAPEGROUPREADERWRITER_HPP
#define SHAPEGROUPREADERWRITER_HPP

#include "shape_reader_writer.hpp"
#include "../shape_factories.hpp"
#include "../shape_group.hpp"

namespace Drawing
{
    namespace IO
    {
        class ShapeGroupReaderWriter : public ShapeReaderWriter
        {
            ShapeFactory& shape_factory_;
            ShapeRWFactory& shape_rw_factory_;
        public:
            ShapeGroupReaderWriter(ShapeFactory& sf, ShapeRWFactory& srwf);

            void read(Shape& shp, std::istream& in) override;

            void write(Shape& shp, std::ostream& out) override
            {

            }
        };
    }
}

#endif // SHAPEGROUPREADERWRITER_HPP
