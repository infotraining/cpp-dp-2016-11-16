#include "shape_group_reader_writer.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

namespace
{
    bool is_registered =
            SingletonShapeRWFactory::instance()
                .register_creator(type_index(typeid(ShapeGroup)),
                                  []{ return make_unique<ShapeGroupReaderWriter>(SingletonShapeFactory::instance(), SingletonShapeRWFactory::instance()); });
}


ShapeGroupReaderWriter::ShapeGroupReaderWriter(ShapeFactory& sf, ShapeRWFactory& srwf)
    : shape_factory_{sf}, shape_rw_factory_{srwf}
{
}

void ShapeGroupReaderWriter::read(Shape& shp, istream& in)
{
    ShapeGroup& sg = static_cast<ShapeGroup&>(shp);

    int count;

    in >> count;

    for(int i = 0; i < count; ++i)
    {
        string shape_id;
        in >> shape_id;

        cout << "Loading " << shape_id << "..." << endl;

        auto shape = shape_factory_.create(shape_id);
        auto shape_rw = shape_rw_factory_.create(type_index{typeid(*shape)});

        shape_rw->read(*shape, in);

        sg.add(move(shape));
    }
}
