#ifndef SHAPEGROUP_HPP
#define SHAPEGROUP_HPP

#include <vector>
#include <memory>

#include "shape.hpp"

namespace Drawing
{

    using ShapePtr = std::unique_ptr<Shape>;

    class ShapeGroup : public CloneableShape<ShapeGroup>
    {
        std::vector<ShapePtr> shapes_;

    public:
        static constexpr const char* id = "ShapeGroup";

        ShapeGroup() = default;

        ShapeGroup(const ShapeGroup& source);

        ShapeGroup& operator=(const ShapeGroup& source);

        void swap(ShapeGroup& other);

        ShapeGroup(ShapeGroup&&) = default;
        ShapeGroup& operator=(ShapeGroup&&) = default;

        void move(int x, int y) override;

        void draw() const override;

        void add(ShapePtr shape)
        {
            shapes_.push_back(std::move(shape));
        }
    };

}


#endif // SHAPEGROUP_HPP
