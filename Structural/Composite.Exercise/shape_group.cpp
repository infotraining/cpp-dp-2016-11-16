#include <algorithm>
#include "shape_group.hpp"
#include "shape_factories.hpp"

using namespace std;
using namespace Drawing;

namespace
{
    bool is_registered =
            SingletonShapeFactory::instance()
                .register_creator(ShapeGroup::id, &make_unique<ShapeGroup>);
}

ShapeGroup::ShapeGroup(const ShapeGroup& source)
{
    for(const auto& shp : source.shapes_)
        shapes_.push_back(shp->clone());
}

ShapeGroup&ShapeGroup::operator=(const ShapeGroup& source)
{
    ShapeGroup temp;
    swap(temp);

    return *this;
}

void ShapeGroup::swap(ShapeGroup& other)
{
    shapes_.swap(other.shapes_);
}

void ShapeGroup::move(int x, int y)
{
    for(const auto& shp : shapes_)
        shp->move(x, y);
}

void ShapeGroup::draw() const
{
    for(const auto& shp : shapes_)
        shp->draw();
}
