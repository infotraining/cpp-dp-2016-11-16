#include <iostream>
#include <vector>
#include <memory>
#include <cassert>
#include <fstream>
#include <unordered_map>
#include <functional>
#include <typeindex>

#include "shape.hpp"
#include "shape_group.hpp"
#include "shape_factories.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

type_index create_type_index(Shape& shape)
{
    return type_index(typeid(shape));
}

class GraphicsDoc
{
    ShapeGroup shapes_;
    ShapeRWFactory& shape_rw_factory_;
public:
    GraphicsDoc(ShapeRWFactory& shape_rw_factory)
            : shape_rw_factory_{shape_rw_factory}
        {}


    void add(unique_ptr<Shape> shp)
    {
        shapes_.add(move(shp));
    }

    void render()
    {
        shapes_.draw();
    }

    void load(const string& filename)
    {
        ifstream file_in{filename};

        if (!file_in)
        {
            cout << "File not found!" << endl;
            exit(1);
        }

        string shape_id;
        file_in >> shape_id;

        assert(shape_id == ShapeGroup::id);

        auto shape_rw = shape_rw_factory_.create(type_index{typeid(ShapeGroup)});

        shape_rw->read(shapes_, file_in);
    }

    void save(const string& filename)
    {
        ofstream file_out{filename};

        auto shape_rw = shape_rw_factory_.create(type_index{typeid(ShapeGroup)});
        shape_rw->write(shapes_, file_out);
    }
};

int main()
{
    cout << "Start..." << endl;

    GraphicsDoc doc(SingletonShapeRWFactory::instance());

    doc.load("drawing.txt");

    cout << "\n";

    doc.render();

    doc.save("new_drawing.txt");
}
