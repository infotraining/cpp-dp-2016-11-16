#include "bitmap.hpp"
#include <vector>

using namespace std;

struct Bitmap::BitmapImpl
{
    vector<char> image_;
};

Bitmap::~Bitmap() = default;

Bitmap::Bitmap(const std::string& filename) : impl_{make_unique<Bitmap::BitmapImpl>()}
{
    impl_->image_.assign(filename.begin(), filename.end());
}

void Bitmap::draw() const
{
    for(const auto& item : impl_->image_)
        cout << item;
    std::cout << std::endl;
}
