#ifndef BITMAP_HPP
#define BITMAP_HPP

#include <string>
#include <iostream>
#include <memory>

class Bitmap
{
public:
    Bitmap(const std::string& filename);
    ~Bitmap();
    void draw() const;
private:
    class BitmapImpl;
    std::unique_ptr<BitmapImpl> impl_;
};

#endif // BITMAP_HPP
